#!/usr/bin/env ruby
require 'csv'

class PreviousElection
  def initialize(result_set)
    @result_set = result_set
  end

  def result
    @result_set.result
  end

  def outcome
    seats_by_party = @result_set.result
    total_seats = 0
    largest_party = ''
    largest_party_seats = 0
    seats_by_party.each do |party, seats|
      total_seats += seats
      if seats > largest_party_seats
        largest_party = party
        largest_party_seats = seats
      end
    end

    seats_required_for_majority = (total_seats/2 ) +1
    if largest_party_seats >=  seats_required_for_majority
      other_parties_seats = total_seats - largest_party_seats
      majority = largest_party_seats - other_parties_seats
      "#{largest_party} majority of #{majority}"
    else
      short = seats_required_for_majority - largest_party_seats
      "Hung parliament (#{largest_party} is #{short} seats short of an overall majority)"
    end
  end

  def results
    @result_set
  end

  def adjust
    @result_set.constituencies.each do |result|
      yield(result)
    end
  end
end

class ConstituencyResultSet
  def initialize(results)
    @constituencies = results
  end

  def constituency(name)
    constituencies = @constituencies.select do |result|
      result.constituency == name
    end

    raise("Unknown constituency #{name}") if constituencies.empty?
    constituencies[0]
  end

  def constituencies
    @constituencies
  end

  def country_filter(country)
    constituencies = @constituencies.select do |result|
      result.country == country
    end

    ConstituencyResultSet.new(constituencies)
  end

  def result
    results = Hash.new(0)
    @constituencies.each do |constituency|
      winner = constituency.winner
      results[winner] = results[winner]+1
    end

    results
  end
end

class ConstituencyResult
  def initialize(raw)
    @constituency = raw['Constituency Name']
    @region = raw['Region']
    @votes = {}
    @country = raw['Country']

    total_votes = raw[' Total number of valid votes counted '].delete(',').to_i
    blank_column_found = false
    raw.each do |row_key, value|
      if blank_column_found
        # Some Labour results are listed under "Lab Co-op"
        row_key = 'Lab' if row_key == 'Lab Co-op'
        vote_count = value.to_i
        percentage_of_vote = 100.0 * vote_count / total_votes
        @votes[row_key] = percentage_of_vote if vote_count > 0
      elsif row_key.nil?
        blank_column_found = true
      end
    end
  end

  def constituency
    @constituency
  end

  def region
    @region
  end

  def country
    @country
  end

  def winner
    @votes.sort_by{|_,v| v}.reverse[0][0]
  end

  def runner_up
    @votes.sort_by{|_,v| v}.reverse[1][0]
  end

  def percentage_vote_for_party(party)
    @votes[party]
  end

  def set_percentage_vote_for_party(party, value)
    @votes[party] = value
  end

  def result
    "#{constituency} (#{region}): Winner: #{winner}"
  end

  def breakdown
    result = ''
    @votes.each do |party, value|
      result += "#{party}:#{value.round(1)} "
    end
    result
  end

  def votes
    @votes
  end
end

def read(file)
  raw = CSV.read(file, headers:true)
  results = []
  raw.each do |raw_result|
    results << ConstituencyResult.new(raw_result) unless raw_result['Constituency Name'].nil?
  end

  PreviousElection.new(ConstituencyResultSet.new(results))
end

previous_election = read('./data/RESULTS FOR ANALYSIS.csv')
puts "2015 UK: #{previous_election.result}"
puts "2015 UK outcome: #{previous_election.outcome}"
puts "2015 England: #{previous_election.results.country_filter('England').result}"
puts "2015 Scotland: #{previous_election.results.country_filter('Scotland').result}"
puts "2015 Wales: #{previous_election.results.country_filter('Wales').result}"
puts ''

# It's possible that UKIP will lose 40% of their vote to the Tories
previous_election.adjust do |result|
  con = result.percentage_vote_for_party('C')
  ukip = result.percentage_vote_for_party('UKIP')
  if con && ukip
    result.set_percentage_vote_for_party('UKIP', ukip * 0.6)
    result.set_percentage_vote_for_party('C', con + (ukip * 0.4))
  end
end

# I reckon that in Tory-held constituencies where the second-place party isn't UKIP, the second-place party
# might get 15% of the votes from the smaller parties, due to tactical voting
previous_election.adjust do |result|
  runner_up = result.runner_up
  if result.winner == 'C' && runner_up != 'UKIP'
    runner_up_share = result.percentage_vote_for_party(runner_up)

    result.votes.each do |party, share|
      if party != 'C' && party != runner_up
        result.set_percentage_vote_for_party(party, share * 0.85)
        runner_up_share += (share * 0.15)
      end
    end

    result.set_percentage_vote_for_party(runner_up, runner_up_share)
  end
end

# I think that SNP will lose 10% of their vote to Labour in Scotland and 5% to Tories
previous_election.adjust do |result|
  snp = result.percentage_vote_for_party('SNP')
  lab = result.percentage_vote_for_party('Lab')
  con = result.percentage_vote_for_party('C')
  if snp && lab && con
    result.set_percentage_vote_for_party('SNP', snp * 0.85)
    result.set_percentage_vote_for_party('Lab', lab + (snp * 0.10))
    result.set_percentage_vote_for_party('C', con + (snp * 0.05))
  end
end

puts "2017 UK: #{previous_election.result}"
puts "2017 UK outcome: #{previous_election.outcome}"
puts "2017 England: #{previous_election.results.country_filter('England').result}"
puts "2017 Scotland: #{previous_election.results.country_filter('Scotland').result}"
puts "2017 Wales: #{previous_election.results.country_filter('Wales').result}"

# breakdown of result for a single constituency
# puts previous_election.results.constituency('Newark').breakdown

# all Scottish seats owned by Con
# previous_election.results.country_filter('Scotland').constituencies.each do |constituency_result|
#   puts constituency_result.constituency if constituency_result.winner == 'C'
# end
