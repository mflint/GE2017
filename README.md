# What?
A Ruby script to play with UK 2015 general election results, to see what might happen in August 2017.

# How?
* Clone repo
* run `2017.rb`

# Data?
* the data is [UK Parliament General Election 7 May 2015 results CSV](http://www.electoralcommission.org.uk/__data/assets/file/0004/191650/2015-UK-general-election-data-results-WEB.zip) from the [Electoral Commision](https://www.electoralcommission.org.uk/our-work/our-research/electoral-data) website

# Swing hypotheses
Random guesses by me about possible swings:
* UKIP lose a load of their vote to the Tories
* Left-wing second-place candidates in Tory-held constituencies will get an increase in vote share, due to "Tories Out" tactical voting
* SNP lose votes to Labour (and Tories, to a lesser extent) compared with 2015 results

# Caveats
* Uses candidate data from 2015, not 2017

# Sample output
```
2015 UK: {"Lab"=>232, "C"=>330, "SNP"=>56, "DUP"=>8, "UUP"=>2, "PC"=>3, "SDLP"=>3, "SF"=>4, "Green"=>1, "Speaker"=>1, "LD"=>8, "UKIP"=>1, "Ind"=>1}
2015 England: {"C"=>318, "Lab"=>206, "Green"=>1, "Speaker"=>1, "LD"=>6, "UKIP"=>1}
2015 Scotland: {"SNP"=>56, "C"=>1, "Lab"=>1, "LD"=>1}
2015 Wales: {"Lab"=>25, "C"=>11, "PC"=>3, "LD"=>1}

2017 UK: {"Lab"=>226, "C"=>341, "SNP"=>51, "DUP"=>8, "UUP"=>2, "PC"=>3, "SDLP"=>3, "SF"=>4, "Green"=>1, "Speaker"=>1, "LD"=>9, "Ind"=>1}
2017 Endland: {"C"=>328, "Lab"=>197, "Green"=>1, "Speaker"=>1, "LD"=>6}
2017 Scotland: {"SNP"=>51, "C"=>2, "LD"=>2, "Lab"=>4}
2017 Wales: {"Lab"=>25, "C"=>11, "PC"=>3, "LD"=>1}

```

# Is this supposed to be serious research?
No